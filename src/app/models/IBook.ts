export interface IBook {
  id:string;
  title:string ;
  price: number;
  imageUrl:string;
}
