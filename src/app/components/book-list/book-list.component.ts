import { Component, OnInit } from '@angular/core';
import { IBook } from 'src/app/models/IBook';
import { BooksService } from 'src/app/services/books.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss'],
})
export class BookListComponent implements OnInit{
  books !: IBook[];

  constructor(private booksService: BooksService){}
  ngOnInit(){
    this.booksService.getAllBooks().subscribe(books => {
        console.log(books)
        this.books = books
    })
  }
}
