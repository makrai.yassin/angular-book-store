import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IBook } from '../models/IBook';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  constructor(private http : HttpClient) {}
  getAllBooks(){
    return this.http.get<IBook[]>("http://localhost:8080/books")
  }
}
